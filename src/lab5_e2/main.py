#!/usr/bin/env python
# import math
# import rospy
# from sensor_msgs.msg import LaserScan

# def callback(data):
    
#     filtered_scan_ranges = [] # the scan ranges we will look at
#     ranges_len = len(data.ranges) # get the indexes of all readings in this scan
#     ranges = ranges_len

#     if ranges_len is 1:
#         ranges = ranges[0] #in case scan did not occur
#     else:
#         #separate the left and right ranges
#         left_lidar_samples_ranges = -(ranges//2 + ranges % 2)
#         right_lidar_samples_ranges = ranges//2

#         left_lidar_samples = data.ranges[left_lidar_samples_ranges:]
#         right_lidar_samples = data.ranges[:right_lidar_samples_ranges]
#         filtered_scan_ranges.extend(left_lidar_samples + right_lidar_samples)


#     for i in range(ranges):
#         if filtered_scan_ranges[i] == float('Inf'):
#             filtered_scan_ranges[i] = 5
#         elif math.isnan(filtered_scan_ranges[i]):
#             filtered_scan_ranges[i] = 0

#     min_distance = min(filtered_scan_ranges)

#     #rospy.loginfo(min_distance)
#     rospy.loginfo(filtered_scan_ranges[2])

# def listener():
#     rospy.init_node('lab5_e2_node', anonymous=True)
#     rospy.Subscriber('/scan', LaserScan, callback)

#     rospy.spin() #prevents node from stopping

# if __name__=='__main__':
#     print("Starting!")
#     listener()
#!/usr/bin/env python

import rospy
import math
import time
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist

# Class holding the logic for the follower application
class Follower:
    def __init__(self):
        #initialize the node for the lab
        # we set anonymous=False so that it closes when you press ctrl+c
        rospy.init_node('lab5_e3_follower', anonymous=False)

        #subscribe to the turtlebot's scan
        self.sub = rospy.Subscriber("/scan", LaserScan, self.callback)

        #publisher to tell the robot to move
        self.turtle_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=10)

        #rate for the turtlebot to publish at
        self.r = rospy.Rate(10) #10Hz

        #called on ctrl+c, DO NOT REMOVE, otherwise node will not exit
        rospy.on_shutdown(self.stophook)

        #flag used to control the publisher based on the rate defined above
        self.can_publish = True
        
        self.z = 0

        #sleep to keep publish rate
        while not rospy.is_shutdown():
            self.r.sleep() #wait
            
    # function to move the turtlebot given an x and z coordinate
    def move_turtlebot(self, x_coord, z_coord=0):
        if self.can_publish:
            #needs to wrapped inside the can_publish flag
            #otherwise turtlebot wil be stuck
            sample_message = Twist()
            sample_message.angular.z = z_coord
            sample_message.linear.x = x_coord
            self.turtle_pub.publish(sample_message)


    #callback to handle the scane request
    #which is sent in the @data parameter
    def callback(self, data):

        ##########
        ## EVERYTHING SUPPLIED IN EXERCISE 2 DOCUMENT
        ##########
        scan_ranges = [] #the scan ranges we will look at
        ranges_len = len(data.ranges)
        ranges = ranges_len

        if ranges_len is 1:
            ranges = ranges[0] #in case the scan did not occur
        else:
            #separate the left and right ranges
            left_lidar_samples_ranges = -(ranges//2 + ranges % 2)
            right_lidar_samples_ranges = ranges//2

            left_lidar_samples = data.ranges[left_lidar_samples_ranges:]
            right_lidar_samples = data.ranges[:right_lidar_samples_ranges]
            scan_ranges.extend(left_lidar_samples + right_lidar_samples)

            filtered_scan_ranges = []
            for i in range(ranges):
                if scan_ranges[i] > data.range_min and scan_ranges[i] < data.range_max:
                    if scan_ranges[i] == float('Inf') or math.isnan(scan_ranges[i]):
                        continue
                    else:
                        filtered_scan_ranges.append(scan_ranges[i])
            
            min_distance = min(filtered_scan_ranges)

            #print out the nearest object
            rospy.loginfo(min_distance)

            ######
            ## IMPLEMENT YOUR LOGIC HERE FOR EXERCISE THREE
            ######
            d = min_distance
            th = filtered_scan_ranges.index(d)
            
            if th > right_lidar_samples_ranges:
                th = -th * data.angle_increment
            else:
                th = th * data.angle_increment
                
            print("left", left_lidar_samples_ranges)
            print("right", right_lidar_samples_ranges)
            d_threshold = 0.3
            x = 1
            
#             print(f"Dist: {d} - Theta: {th}")
            alpha = lambda a: 0 <= a <= 90
            z = self.z
            if (abs(int(d) - d_threshold) > 1 and alpha(th)) or (th != z and alpha(th)):
                # moving state
                if d > d_threshold and abs(z - th) < (2 * data.angle_increment):
                    print("GOING STRAIGHT\n")
                    x = x
                elif d < d_threshold and abs(z - th) < (2 * data.angle_increment):
                    print("GOING BACK\n")
                    x = -x
                elif d > d_threshold and abs(z - th) > (2 * data.angle_increment):
                    print(f"GOING STRAIGHT AT AN ANGLE {z} - {th}\n")
                    x = x
                elif d < d_threshold and abs(z - th) > (2 * data.angle_increment):
                    print(f"GOING BACK AT AN ANGLE {z} - {th}\n")
                    x = -x
                elif abs(int(d) - d_threshold) > 1 and abs(z - th) > (2 * data.angle_increment):
                    print(f"TURN AT AN ANGLE {z} - {th}\n")
                    x = 0
                z = th
            else:
                print("IDLE\n")
                x = 0
                z = 0
            
            self.z = z


            #hint: you can use the left_lidar_samples array and right_lidar_samples array above to 
            # determine the angle the leader is in

            #another hint: it will be easier if you limit/ require the leader to be in the angle range 0 - 90
            # of the turtlebot, so that the robot turns to face the leader, and then moves forward (i.e., x > 0)

            #call the move function with the new x and z values
            self.move_turtlebot(x, z)

    #callback to stop the code and node process
    #DO NOT REMOVE :/
    def stophook(self):
        # stop turtlebot from moving
        self.can_publish = False
        rospy.loginfo("Stop TurtleBot")

        #publish empty Twist to stop movement
        self.turtle_pub.publish(Twist())

        rospy.sleep(1)

#start up the follower application
if __name__=='__main__':
    try:
        Follower()
    except:
        rospy.loginfo("GoForward node terminated.")

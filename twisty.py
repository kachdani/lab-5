import rospy
from geometry_msgs.msg import Twist
rospy.init_node("sample_lab5_e2", anonymous=True)
turtle_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=10)

sample_message = Twist()
sample_message.angular.z = 1
sample_message.linear.x = .5

turtle_pub.publish(sample_message)
